package com.xa.aria.TechTestSubdev.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xa.aria.TechTestSubdev.models.PositionModel;

public interface PositionRepository extends JpaRepository<PositionModel,Integer> {
	
	List<PositionModel> findByIsDelete(Integer isDelete);
}
