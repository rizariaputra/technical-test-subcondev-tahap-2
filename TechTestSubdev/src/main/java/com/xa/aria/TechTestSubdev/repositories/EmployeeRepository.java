package com.xa.aria.TechTestSubdev.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xa.aria.TechTestSubdev.models.EmployeeModel;

public interface EmployeeRepository extends JpaRepository<EmployeeModel,Integer> {
	
	List<EmployeeModel> findByIsDelete(Integer isDelete);
}
