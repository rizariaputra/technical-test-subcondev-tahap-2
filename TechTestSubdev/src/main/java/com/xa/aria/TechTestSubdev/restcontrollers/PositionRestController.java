package com.xa.aria.TechTestSubdev.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.aria.TechTestSubdev.services.PositionService;

@RestController
@RequestMapping(path = "/api/position", produces = "application/json")
@CrossOrigin(origins = "*")
public class PositionRestController {
	@Autowired
	private PositionService positionService;
	
	@GetMapping("/")
	public ResponseEntity<?>findByIsDelete(Integer isDelete){
		return new ResponseEntity<>(positionService.findByIsDelete(0), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(positionService.findById(id), HttpStatus.OK);
	}
}
