package com.xa.aria.TechTestSubdev.daos;

import java.util.List;
import java.util.Optional;

import com.xa.aria.TechTestSubdev.models.PositionModel;

public interface PositionDAO {
	List<PositionModel> findByIsDelete(Integer isDelete);

	Optional<PositionModel> findById(Integer id);
}
