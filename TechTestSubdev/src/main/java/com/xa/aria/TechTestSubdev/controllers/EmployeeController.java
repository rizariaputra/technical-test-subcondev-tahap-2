package com.xa.aria.TechTestSubdev.controllers;

import com.xa.aria.TechTestSubdev.models.EmployeeModel;
import com.xa.aria.TechTestSubdev.services.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;
    
    @GetMapping("index")
    public String index() {
        return "employee/index";
    }

    @GetMapping("add")
    public String add() {
        return "employee/add";
    }

    @GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Integer id) {
		ModelAndView view = new ModelAndView("/employee/edit");
		EmployeeModel employee = this.employeeService.findById(id).orElse(null);
        view.addObject("employee", employee);
			return view;
    }

    @RequestMapping(path = "/trigger-error", produces = MediaType.APPLICATION_JSON_VALUE)
    public void error500() throws Exception {
        throw new Exception();
    }
}