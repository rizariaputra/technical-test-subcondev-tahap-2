package com.xa.aria.TechTestSubdev.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="t2_employee")
public class EmployeeModel {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable = false)
    private Integer id;
    
    @Column(name="name", nullable = false, length = 100)
    private String name;
    
	@Column(name="birth_date", nullable = false)
	@JsonFormat(pattern="dd/MM/yyyy")
    private Date birthDate;
    
	@Column(name="position_id", nullable = false)
    private Integer positionId;
    
    @ManyToOne
	@JoinColumn(name="position_id", insertable = false, updatable = false)
	public PositionModel positionModel;
    
    @Column(name="id_number", nullable = false)
    private Integer idNumber;
    
    @Column(name="gender", nullable = false)
    private Integer gender;
    
    @Column(name="is_delete", nullable = false)
    private Integer isDelete;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Integer getPositionId() {
		return positionId;
	}

	public void setPositionId(Integer positionId) {
		this.positionId = positionId;
	}

	public PositionModel getPositionModel() {
		return positionModel;
	}

	public void setPositionModel(PositionModel positionModel) {
		this.positionModel = positionModel;
	}

	public Integer getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(Integer idNumber) {
		this.idNumber = idNumber;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

    
}
