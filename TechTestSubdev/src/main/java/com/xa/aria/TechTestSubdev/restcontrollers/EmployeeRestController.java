package com.xa.aria.TechTestSubdev.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.aria.TechTestSubdev.models.EmployeeModel;
import com.xa.aria.TechTestSubdev.services.EmployeeService;

@RestController
@RequestMapping(path = "/api/employee", produces = "application/json")
@CrossOrigin(origins = "*")
public class EmployeeRestController {
	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping("/")
	public ResponseEntity<?>findByIsDelete(Integer isDelete){
		return new ResponseEntity<>(employeeService.findByIsDelete(0), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> saveEmployee(@RequestBody EmployeeModel employee){
		return new ResponseEntity<>(employeeService.save(employee), HttpStatus.OK);
	}
	
	@PutMapping("/put")
	public ResponseEntity<?> editEmployee(@RequestBody EmployeeModel employee) {
		return new ResponseEntity<>(employeeService.save(employee), HttpStatus.OK);
	}
	
	@PutMapping("/delete")
	public ResponseEntity<?> deleteEmployee(@RequestBody EmployeeModel employee) {
		return new ResponseEntity<>(employeeService.save(employee), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(employeeService.findById(id), HttpStatus.OK);
	}
}
