package com.xa.aria.TechTestSubdev.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xa.aria.TechTestSubdev.daos.PositionDAO;
import com.xa.aria.TechTestSubdev.models.PositionModel;
import com.xa.aria.TechTestSubdev.repositories.PositionRepository;

@Service
public class PositionService implements PositionDAO {
	@Autowired
	private PositionRepository positionRepository;

	@Override
	public List<PositionModel> findByIsDelete(Integer isDelete) {
		return positionRepository.findByIsDelete(isDelete);
	};

	@Override
	public Optional<PositionModel> findById(Integer id) {
		return positionRepository.findById(id);
	}
}
