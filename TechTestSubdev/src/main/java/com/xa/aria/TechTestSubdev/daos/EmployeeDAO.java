package com.xa.aria.TechTestSubdev.daos;

import java.util.List;
import java.util.Optional;

import com.xa.aria.TechTestSubdev.models.EmployeeModel;

public interface EmployeeDAO {
	
	List<EmployeeModel> findByIsDelete(Integer isDelete);
	
	EmployeeModel save(EmployeeModel employee);
	
	Optional<EmployeeModel> findById(Integer id);
	
}
