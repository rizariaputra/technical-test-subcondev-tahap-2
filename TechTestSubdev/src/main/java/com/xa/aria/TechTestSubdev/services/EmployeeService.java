package com.xa.aria.TechTestSubdev.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xa.aria.TechTestSubdev.daos.EmployeeDAO;
import com.xa.aria.TechTestSubdev.models.EmployeeModel;
import com.xa.aria.TechTestSubdev.repositories.EmployeeRepository;

@Service
public class EmployeeService implements EmployeeDAO{
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Override
	public List<EmployeeModel> findByIsDelete(Integer isDelete){
		return employeeRepository.findByIsDelete(isDelete);
	};
	
	@Override
	public EmployeeModel save(EmployeeModel employee) {
		return employeeRepository.save(employee);
	}
	
	@Override
	public Optional<EmployeeModel> findById(Integer id) {
		return employeeRepository.findById(id);
	}
	
}
