package com.xa.aria.TechTestSubdev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechTestSubdevApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechTestSubdevApplication.class, args);
	}

}
